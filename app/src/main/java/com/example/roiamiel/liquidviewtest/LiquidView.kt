package com.example.roiamiel.liquidviewtest

import android.content.Context
import android.graphics.*
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.example.roiamiel.liquidviewtest.LiquidView.DisplayThread.Companion.MIN_DELAY_TIME

class LiquidView: SurfaceView, SurfaceHolder.Callback, SensorEventListener {

    companion object {
        private const val MAX_ANGLE = 0.977
    }

    constructor(context: Context): super(context)

    constructor(context: Context, attr: AttributeSet): super(context, attr)

    constructor(context: Context, attr: AttributeSet, defStyleA: Int): super(context, attr, defStyleA)

    private var displayThread: DisplayThread? = null

    private val sensorManager: SensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val accelerometerSensor: Sensor

    private var lastAngle = 0.0
    private var lastUpdateTime = -1L

    private val paint = Paint().apply { color = Color.RED; strokeWidth = 10f }

    init {
        // install accelerometer sensor
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        if (accelerometerSensor == null) {
            // todo draw basic liquid

        } else {
            holder.addCallback(this)
            displayThread = DisplayThread(holder, this)
            isFocusable = true
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        // install sensor listener
        sensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        // remove sensor listener
        sensorManager.unregisterListener(this)
    }

    fun update() {

    }

    fun render(c: Canvas) {
        c.drawColor(Color.BLACK)

        val w = c.width.toFloat()
        val h = c.height.toFloat()

        val halfW = w / 1.8f
        val halfH = h / 1.8f

        val angle = if (Math.abs(lastAngle) < MAX_ANGLE) lastAngle else MAX_ANGLE * (lastAngle / Math.abs(lastAngle))
        val cosTop = (Math.cos(angle) * w).toFloat()
        val sinTop = (Math.sin(angle) * w).toFloat()

        val fillPath = Path().apply {
            moveTo(halfW - cosTop, halfH - sinTop)
            lineTo(halfW + cosTop, halfH + sinTop)
            lineTo(halfW + cosTop, halfH + sinTop + h * 2)
            lineTo(halfW - cosTop, halfH - sinTop + h * 2)
            lineTo(halfW - cosTop, halfH - sinTop)
        }

        c.drawPath(fillPath, paint)
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {}
    override fun onSensorChanged(event: SensorEvent) {
        // sync the delay time between frames to the time that it takes to the sensor to provide new value
        val time = System.currentTimeMillis()
        if (lastUpdateTime != -1L && displayThread != null) {
            displayThread!!.delay = Math.max(MIN_DELAY_TIME.toDouble(), (time - lastUpdateTime) * 0.8).toLong()
        }
        lastUpdateTime = time

        // calculate the angle with the ground
        val normalZ = event.values[0] / Math.sqrt((event.values[0] * event.values[0] + event.values[1] * event.values[1] + event.values[2] * event.values[2]).toDouble())
        val angle = (Math.PI / 2 - Math.acos(normalZ) + lastAngle) / 2.0

        lastAngle = angle
    }

    override  fun surfaceCreated(holder: SurfaceHolder) {
        //Starts the display thread
       if(displayThread == null || !displayThread!!.isRunning) displayThread = DisplayThread(holder, this).apply { start() }
       else displayThread!!.start()
    }

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        if (displayThread == null) return

        displayThread!!.isRunning = false

        // stop the thread
        var retry = true
        while (retry) {
            try {
                displayThread!!.join()
                retry = false

            } catch (_: InterruptedException) {}
        }
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}

    /**
     * Responsible for screen painting.
     * */
    class DisplayThread(private val surfaceHolder: SurfaceHolder, 
                        private val liquidView: LiquidView) : Thread() {

        companion object {
            const val MIN_DELAY_TIME = 10L
        }

        // Delay amount between screen refreshes
        var delay = MIN_DELAY_TIME

        // flags
        var isRunning = true

        /**
         * This is the main nucleus of our program.
         * From here will be called all the method that are associated with the display in GameEngine object
         * */
        override fun run() {
            //Looping until the boolean is false
            while (isRunning) {
                // update view
                liquidView.update()

                //locking the canvas
                surfaceHolder.lockCanvas(null)?.also {
                    //Clears the screen with black paint and draws object on the canvas
                    synchronized (surfaceHolder) { liquidView.render(it) }

                    //unlocking the Canvas
                    surfaceHolder.unlockCanvasAndPost(it)
                }

                // put delay until next frame
                Thread.sleep(delay)
            }
        }
    }


}